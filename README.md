Snips front-end challenge app
=============================

![caption](/caption.png)

# Achieved

- Resizable slots
- Slot counter (intent wise)
- No overlapping slots
- Editable utterance

# How to

```bash
git clone https://bitbucket.org/elbywan/snipsapp.git
cd snipsapp
npm install   # install dependencies
npm run dev   # dev server with hot reload @ http://localhost:8080/
npm run prod  # production build served    @ http://localhost:8080/
```

# Tech stack

- React / redux -> web framework & model
- Typescript    -> language
- Fusebox       -> deployment
- Sass          -> style

# Approach and difficulties

1. Retrieve the caret according to the mouse position and retrieve the portion of the DOM

    This was not very difficult to find out, the [Range API](https://developer.mozilla.org/en-US/docs/Web/API/Range) was pretty much designed for this. I chose to mark on mouse click the slot and on mouse move to [get the caret position](https://developer.mozilla.org/en-US/docs/Web/API/Document/caretPositionFromPoint).
    Then I "diff" the new and old text using the Range API to get the underlying DOM fragment.

2. How to reflect these changes into the data "state"

    This was actually the most difficult part. As the slot fragment can grow or shrink, the neighboring fragments must be resized accordingly. There are a lot of edge cases such as resizing a slot in position 0 or end position, or preventing overlap.
    Basically I chose to recreate from scratch the entire utterance data object by calculating the new text and range based on the slot index and the diff.

3. Storing data properly

    I used a very basic "redux" approach here, but a single state store located in the root component would be similar.

4. The drag visuals

    I chose to put two vertical bars | at the sides of the slots. 2 difficulties here :
    - No text inside the bars since the utterance text must remain untouched
    - No absolute positioning because of possible line breaks (and also some browsers do not recalculate the position properly and cause delays)

5. Edit the utterance

    I made each fragment of the utterance "contenteditable", and listened to the onInput events. The utterance is then updated with the new text and data mapped directly from the dom.

    I did not make the whole utterance editable because react would then update the fragments and the caret would move around wildly.
    Also, line breaks are ignored because they would insert new nodes in the utterance and introduce entropy.

# Project architecture

```bash
├── /dist            # Build directory
│
└── /src             # Sources
     │
     ├── /components # React components
     │
     ├── /data       # The json data
     │
     ├── /store      # Redux store, action, reducers
     │               # and api to retrieve data
     │
     └── /tools      # Various useful tools
```

# Next steps ?

- Add / remove slots in an utterance

Removal is easy.
For the addition, use Document.getSelection() and check for overlaps.
In both cases, regenerate the utterance data & text.

- Add / remove new slots, utterance and intents

No logic here, just plain old redux boilerplate :-).