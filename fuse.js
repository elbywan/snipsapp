const { FuseBox, Sparky, EnvPlugin, SassPlugin,
    CSSPlugin, WebIndexPlugin, QuantumPlugin, CopyPlugin } = require("fuse-box")

let fuse, vendor, app, production

Sparky.task("config", () => {
    fuse = FuseBox.init({
        homeDir: "src",
        output: "dist/$name.js",
        package: "snipsapp",
        hash: production,
        cache: !production,
        sourceMaps: true,
        useJsNext: false,
        alias: {
            "self": "~"
        },
        plugins: [
            EnvPlugin({ NODE_ENV: production ? "production" : "development" }),
            [ SassPlugin(), CSSPlugin() ],
            WebIndexPlugin({
                template: "src/index.html"
            }),
            production && QuantumPlugin({
                treeshake: true,
                uglify: true
            }),
            CopyPlugin({ files: [ "*.json" ]})
        ]
    })

    vendor = fuse.bundle("vendor").target("browser").instructions("~ index.tsx")
    app = fuse.bundle("app").target("browser").instructions("> [index.tsx]")
})

Sparky.task("clean", () => Sparky.src("dist/").clean("dist/"))
Sparky.task("prod-env", ["clean"], () => { production = true })
Sparky.task("build", ["prod-env", "config"], () => fuse.run())
Sparky.task("default", ["clean", "config"], () => {
    fuse.dev({
        port : 8080,
        root: "dist"
    })
    app.watch().hmr({ reload: true })
    return fuse.run()
})