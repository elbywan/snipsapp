import * as React from "react"
import "./SlotsLegend.scss"

const countSlot = (slot, utterances) =>
    utterances.reduce((accumulator, utterance) =>
        accumulator + utterance.data.filter(_ => _.slot_id === slot.id).length, 0)

export const SlotsLegend = ({ slots, utterances}) =>
    <div className="slots-legend">
        <h3>Slots</h3>
        { slots && slots.map((slot, idx) =>
            <div key={ slot.id } className="slot">
                <label>{ slot.name }</label>
                <div className="color-box" style={{ backgroundColor: slot.color }}></div>
                <div className="companion-text">Used { countSlot(slot, utterances) } times.</div>
            </div>
        )}
    </div>