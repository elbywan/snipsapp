import * as React from "react"
import { connect } from "react-redux"
import * as actions from "self/store/actions"
import { Intent } from "self/components"
const travelIntent  = require("self/data/travelIntent.json").default
const wretch = require("wretch")

export class _App extends React.PureComponent<{ intents: any[], onLoad: any }> {

    componentDidMount() {
        // Initial store load
        wretch(travelIntent)
            .get()
            .json(this.props.onLoad)
    }

    render() {
        return (
            <div className="app">
                { this.props.intents.map(intent =>
                    <Intent key={ intent.intent.name } intent={ intent } />)}
            </div>
        )
    }

}

export const App = connect(
    ({ intents }) => ({ intents }),
    dispatch => ({ onLoad: json => dispatch(actions.loadIntent(json)) })
)(_App)