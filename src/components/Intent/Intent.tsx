import * as React from "react"
import { connect, DISPATCH } from "react-redux"
import * as actions from "self/store/actions"
import { Utterance, SlotsLegend } from "self/components"

import "./Intent.scss"

class _Intent extends React.PureComponent<{ intent: any, dispatch: DISPATCH }> {

    componentDidMount() {
        document.addEventListener("mouseup", this.stopExpand)
    }

    componentWillUnmount() {
        document.removeEventListener("mouseup", this.stopExpand)
    }

    doExpand(event) {
        if(this.props.intent.expandInfo) {
            this.props.dispatch(actions.doSlotResize({
                event,
                intent: this.props.intent
            }))
        }
    }

    stopExpand = event => {
        this.props.dispatch(actions.endSlotResize(this.props.intent.intent.name))
    }

    render() {
        const { intent, slots, utterances } = this.props.intent
        return (
            <div className="card intent">
                <h1>{ intent.name }</h1>
                <SlotsLegend slots={ slots } utterances={ utterances } />
                <div className="utterances" onMouseMove={ e => this.doExpand(e) } >
                    <h3>Utterances</h3>
                    { utterances && utterances.map((utterance, idx) =>
                        <Utterance  key={ idx } intent={ this.props.intent } utterance={ utterance }/>
                    )}
                </div>
            </div>
        )
    }
}

export const Intent = connect()(_Intent)