import * as React from "react"
import * as actions from "self/store/actions"
import * as store from "self/store"
import * as tools from "self/tools"
import { connect, DISPATCH } from "react-redux"

import "./Utterance.scss"

class _Utterance extends React.PureComponent<{ intent: any, utterance: any, dispatch: DISPATCH }> {

    utteranceNode = null

    startExpand(event, slot, start = false) {
        this.props.dispatch(actions.startSlotResize({
            referenceNode: event.currentTarget.parentNode.childNodes[1],
            intentName: this.props.intent.intent.name,
            utteranceText: this.props.utterance.text,
            slotId: slot.slot_id,
            start
        }))
    }

    changeUtterance(event) {
        if(!this.utteranceNode) return
        const newText = this.utteranceNode.textContent
        const nodeList = this.utteranceNode.childNodes
        const utterance = this.props.utterance
        const intent = this.props.intent
        this.props.dispatch(actions.updateUtterance({ intent, utterance, newText, nodeList }))
    }

    isExpanding(slot_id) {
        const { expandInfo } = this.props.intent
        return expandInfo &&
            expandInfo.utteranceText === this.props.utterance.text &&
            expandInfo.referenceNode.parentNode.attributes.getNamedItem("data-slot-id").value === slot_id ?
                expandInfo.start ?
                    " expanding expanding-start" :
                    " expanding expanding-end" :
                ""
    }

    renderUtteranceFragment = (frag, idx) => {
        const { slots, utterances } = this.props.intent

        if(!frag.slot_id) {
            return <span key={ idx } contentEditable={ true } suppressContentEditableWarning={ true }>{ frag.text }</span>
        }

        const color = slots && store.getSlotById(slots, frag.slot_id).color

        return (
            <span   className={ "slot"  + this.isExpanding(frag.slot_id) }
                    data-slot-id={ frag.slot_id }
                    key={ frag.slot_id }
                    style={{ backgroundColor: color, color: tools.foregroundColor(color) }}>
                <span className="slot-expander slot-start" onMouseDown={ e => this.startExpand(e, frag, true) } ></span>
                <span contentEditable={ true } suppressContentEditableWarning={ true }>{ frag.text }</span>
                <span className="slot-expander slot-end" onMouseDown={ e => this.startExpand(e, frag) }></span>
            </span>
        )
    }

    render() {
        return (
            <div    className="utterance"
                    onKeyPress={ e => e.which === 13 ? e.preventDefault() : true }
                    onBlur={ e => this.changeUtterance(e) }
                    ref={ ref => this.utteranceNode = ref }>
                { this.props.utterance.data.map(this.renderUtteranceFragment) }
            </div>
        )
    }
}

export const Utterance = connect()(_Utterance)