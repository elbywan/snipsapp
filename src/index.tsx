import * as React from "react"
import * as ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { App } from "./components"

import store from "self/store"
import "./index.scss"

ReactDOM.render(<Provider store={ store }><App /></Provider>, document.getElementById("app-root"))