import * as actions from "self/store/actions"
import * as store from "self/store"

export default (intents, action) => {
    switch(action.type) {
        case actions.ADD_INTENT:
            return [ ...intents, action.data ]
        case actions.START_SLOT_RESIZE:
            return intents.map(_ => _.intent.name === action.data.intentName ? startSlotResize(_, action) : _)
        case actions.DO_SLOT_RESIZE:
            return intents.map(_ => _.intent.name === action.intentName ? doSlotResize(_, action) : _)
        case actions.END_SLOT_RESIZE:
            return intents.map(_ => _.intent.name === action.intentName ? endSlotResize(_, action) : _)
        case actions.UPDATE_UTTERANCE :
            return intents.map(_ => _.intent.name === action.intentName ? updateUtterance(_, action): _)
        default:
            return intents
    }
}

const startSlotResize = (intent, action) => {
    return { ...intent, expandInfo: action.data }
}

const doSlotResize = (intent, action) => {
    const utterance = store.getUtteranceByText(intent.utterances, action.utteranceText)
    return {
        ...intent,
        utterances: intent.utterances.map(u =>
            u === utterance ? { ...u, data: action.data } : u
        )
    }
}

const endSlotResize = (intent, action) => {
    return { ...intent, expandInfo: null }
}

const updateUtterance = (intent, action) => {
    return { ...intent, utterances: intent.utterances.map(u =>
        u.text === action.utteranceText ?
            action.utterance :
            u
    )}
}