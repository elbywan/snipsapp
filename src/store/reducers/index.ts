import reduceIntents from "./intents"

export default (state = { intents: [] }, action) => ({
    intents: reduceIntents(state.intents, action)
})