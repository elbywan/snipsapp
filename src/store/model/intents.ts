export const getIntentByName = (intents, name) => intents && intents.find(_ => _.intent.name === name)
export const getUtteranceByText = (utterances, text) => utterances && utterances.find(_ => _.text === text)
export const getSlotById = (slots, id) => slots && slots.find(_ => _.id === id)
export const getUtteranceSlotById = (utterance, id) => utterance && utterance.data.find(_ => _.slot_id === id)