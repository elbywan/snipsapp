import { createStore, applyMiddleware, Store } from "redux"
import thunk from "redux-thunk"
import rootReducer from "./reducers"

export * from "./model"

export default createStore(
    rootReducer,
    applyMiddleware(thunk)
)