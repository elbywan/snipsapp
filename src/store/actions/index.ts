import * as tools from "self/tools"
import * as store from "self/store"
import { generateUtteranceData, generateUtteranceDataFromNodes } from "./helpers"

export const ADD_INTENT = "LOAD_INTENT"
export const START_SLOT_RESIZE = "START_SLOT_RESIZE"
export const DO_SLOT_RESIZE = "DO_SLOT_RESIZE"
export const END_SLOT_RESIZE = "END_SLOT_RESIZE"
export const UPDATE_UTTERANCE = "UPDATE_UTTERANCE"

// Initial loading
export const loadIntent = json => {
    const data = {
        ...json,
        slots: json.slots.map(_ => ({
            ..._,
            color: tools.randomColor()
        }))
    }
    return {
        type: ADD_INTENT,
        data: data
    }
}

// Memorizes data used during a slot resize
export const startSlotResize = ({ referenceNode, intentName, utteranceText, slotId, start = false }) => {
    return {
        type: START_SLOT_RESIZE,
        data: {
            referenceNode,
            intentName,
            utteranceText,
            slotId,
            start
        }
    }
}

// Removes data
export const endSlotResize = intentName => {
    return {
        type: END_SLOT_RESIZE,
        intentName: intentName
    }
}

// Resizes a slot
export const doSlotResize = ({ event, intent }) => {
    // Data retrieval

    // Get caret relative to the mouse position
    let { range: cursorRange, node, offset } = tools.getCaret(event)
    // Retrieve the start resize data
    const { start, referenceNode, utteranceText, slotId } = intent.expandInfo
    // Related utterance & slot
    const utterance = store.getUtteranceByText(intent.utterances, utteranceText)
    const slot = store.getUtteranceSlotById(utterance, slotId)

    // Create the range between the starting point and the mouse position
    const range = document.createRange()

    if(start) {
        // If we use the "left" drag pin
        range.setEndAfter(referenceNode)
        // Prevents slots overlapping
        if(referenceNode.parentElement.contains(node) ||
                referenceNode.parentElement.previousSibling &&
                referenceNode.parentElement.previousSibling.contains(node))
            range.setStart(node, offset)
        else
            return { type: "SLOT_OVERLAP" }
    }
    else {
        // Right drag pin
        range.setStartBefore(referenceNode)
        // Prevents slots overlapping
        if(referenceNode.parentElement.contains(node) ||
                referenceNode.parentElement.nextSibling &&
                referenceNode.parentElement.nextSibling.contains(node))
            range.setEnd(node, offset)
        else
            return { type: "SLOT_OVERLAP" }
    }

    // Analyze the range to determine if the new text is bigger

    const rangeDocument = range.cloneContents()
    const newText = rangeDocument.textContent
    const big = newText.length > slot.text.length ? newText : slot.text
    const small = newText.length > slot.text.length ? slot.text : newText
    const diff = start ? big.substring(0, big.length - small.length) : big.substring(small.length)

    // Generate brand new utterance data

    const data = generateUtteranceData(utterance, slot, newText, diff, big === newText, start)

    // Replace the current data with the new one

    return {
        type: DO_SLOT_RESIZE,
        intentName: intent.intent.name,
        utteranceText,
        data: data
    }
}

// Update an utterance with a new text and data
export const updateUtterance = ({ intent, utterance, newText, nodeList }) => ({
    type: UPDATE_UTTERANCE,
    intentName: intent.intent.name,
    utteranceText: utterance.text,
    utterance: {
        ...utterance,
        text: newText,
        data: generateUtteranceDataFromNodes(nodeList)
    }
})