import * as tools from "self/tools"

// Generate new utterance data fragments based on old and new slot text
export const generateUtteranceData = (utterance, slot, newText: string, diff: string, widen: boolean, start: boolean) => {
    // Incremental fragment range
    let range = 0
    // Index of the slot being resized
    let index = utterance.data.indexOf(slot)

    // Generate the new range based on the text length
    const generateRange = text => {
        const oldRange = range
        range = range + text.length
        return { range: { start: oldRange, end: range }}
    }
    const newData = utterance.data

    // Add a text node at the beginning / end of the utterance when resizing a "border" slot
    if(!widen && start && index === 0) {
        newData.unshift({ text: "" })
        index++
    } else if(!widen && !start && index === utterance.data.length - 1) {
        newData.push({ text: "" })
    }

    // Generates new slots and text fragments
    return newData.map((data, idx) => {
        // The modified slot
        if(idx === index) {
            return {
                ...data,
                text: newText,
                ...generateRange(newText)
            }
        }
        // If we used the left pin, this is the fragment on the left of the slot
        if(start && idx === index - 1) {
            const text = widen ?
                data.text.substring(0, data.text.lastIndexOf(/*truncDiff*/diff)) :
                data.text + diff
            return {
                ...data,
                text: text,
                ...generateRange(text)
            }
        }
         // If we used the right pin, this is the fragment on the right of the slot
        if(!start && idx === index + 1){
            const text = widen ?
                data.text.substring(data.text.indexOf(diff.substring(0, data.text.length)) + diff.length) :
                diff + data.text
            return {
                ...data,
                text: text,
                ...generateRange(text)
            }
        }
        // Other fragments
        return {
            ...data,
            ...generateRange(data.text)
        }
    }).filter((_, i) =>
        // "Trim" empty fragments at the boundaries of the utterance
        _.slot_id || _.range.start < _.range.end || (i > 0 && i < newData.length - 1)
    )
}

// Generate new data from DOM nodes
export const generateUtteranceDataFromNodes = (nodeList: NodeList) => {
    let range = 0

    return tools.nodeListToArray(nodeList).map((node, idx) => {
        const fragment = {
            text: node.textContent,
            range: {
                start: range,
                end: range + node.textContent
            }
        }
        if(node.nodeType !== 3 && node.attributes.getNamedItem("data-slot-id")) {
            fragment["slot_id"] = node.attributes.getNamedItem("data-slot-id").value
        }
        range += fragment.text.length

        return fragment
    })
}