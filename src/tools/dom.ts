// Transform a NodeList to an Array
export const nodeListToArray = (nodeList: NodeList) : Array<Node> => {
    const array = []
    for(let i = 0; i < nodeList.length; i++)
        array.push(nodeList[i])
    return array
}

// Returns a random color (not too dark nor too bright)
export const randomColor = () => {
    const color = ((1 << 24) * Math.random() & 0xF0F0F0 | 0x0F0F0F).toString(16)
    const pad = (str, length) => str.length < length ? pad("0" + str, length) : str
    return "#" + pad(color, 6)
}

// White or black depending on the background color contrast
export const foregroundColor = (background, threshold = 180) => {
    const c = parseInt(background.substring(1), 16)
    if((c >> 16) + ((c & 0x00FF00) >> 8) + (c & 0x0000FF) >= threshold * 3)
        return "black"
    return "white"
}