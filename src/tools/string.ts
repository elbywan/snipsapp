// Truncates a string at max "length" characters (from the right)

export const truncateRight = (str, length) => {
    if(str.length > length) {
        return str.substring(str.length - length)
    }
    return str
}