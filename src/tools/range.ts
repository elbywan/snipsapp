// Cross browser mouse position caret

export const getCaret = event => {
    let range, offset, node

    if (document["caretPositionFromPoint"]) {
        range = document["caretPositionFromPoint"](event.clientX, event.clientY)
        node = range.offsetNode
        offset = range.offset
    } else if (document.caretRangeFromPoint) {
        range = document.caretRangeFromPoint(event.clientX, event.clientY)
        node = range.startContainer
        offset = range.startOffset
    }

    return { range, offset, node }
}